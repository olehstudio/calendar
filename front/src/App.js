import React, { useState, useEffect } from "react";
import styled from "styled-components";
import axios from "axios";
import { Calendar } from "antd";
import moment from "moment";
import "antd/dist/antd.css";
import { CloseOutlined } from "@ant-design/icons";

import Timer from "./components/Timer";
import AddForm from "./components/AddForm";
import { convertDateToID, firstLetterUppercase } from "./utils/utils";

const CalendarContainer = styled.div`
    display: block;
    width: 100%;
    max-width: 1440px;
    padding: 15px;
    border: 1px solid #f0f0f0;
    border-radius: 7px;
    margin: 50px auto 0;
`;

const StopwatchContainer = styled.div`
    display: flex;
    justify-content: center;
    margin: 20px 0 0 0;
`;

const EventsList = styled.ul`
    padding: 0;
    list-style: none;

    li {
        display: flex;
        align-items: center;
        padding: 0 8px 0 15px;
        font-weight: 300;
        color: #fff;
        background-color: #1890ff;
        border-radius: 5px;
        margin-bottom: 3px;
    }
`;

const StyledCross = styled(CloseOutlined)`
    margin-left: auto;
`;

function App() {
    const [datesData, setDatesData] = useState({});
    const [month, setMonth] = useState(0);

    useEffect(() => {
        axios.get(`http://localhost:3001/records/${month}`).then(response => {
            console.log(response);
            if (response.data.length) {
                setDatesData(response.data);
            }
        });
    }, [month]);

    const handleClickRemoveTask = e => {
        axios
            .delete("http://localhost:3001/record", {
                data: {
                    dateID: Number(e.target.parentNode.getAttribute("data-dataid")),
                    timeType: e.target.parentNode.getAttribute("data-timetype")
                }
            })
            .then(function(response) {
                console.log(response);
                setMonth(0);
            });
    };

    function dateCellRender(value) {
        if (!month) {
            setMonth(Number(value.format("YYYYMM")));
        }

        const dateID = convertDateToID(value);

        if (datesData.length) {
            return (
                <EventsList className="events">
                    {datesData
                        .filter(object => object.dateID === dateID)
                        .map(event => (
                            <li key={`${event.id}${event.dataID}`}>
                                {firstLetterUppercase(event.timeType)}:{" "}
                                {moment
                                    .utc(event.timeSpent * 1000)
                                    .format("HH:mm:ss")}
                                <StyledCross
                                    data-dataid={event.dateID}
                                    data-timetype={event.timeType}
                                    onClick={handleClickRemoveTask}
                                />
                            </li>
                        ))}
                </EventsList>
            );
        }
    }

    return (
        <>
            <CalendarContainer>
                <Calendar dateCellRender={dateCellRender} />
            </CalendarContainer>

            <StopwatchContainer>
                <Timer type="job" setMonth={setMonth} />
                <Timer type="learning" setMonth={setMonth} />
                <Timer type="work" setMonth={setMonth} />
            </StopwatchContainer>

            <AddForm setMonth={setMonth} />
        </>
    );
}

export default App;
