const ACTIONS = {
    TIME: {
        SET_TIMES: "SET_TIMES"
    }
};

export default Object.seal(ACTIONS);
