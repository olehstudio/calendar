import ACTIONS from "./actionTypes";

export const setTimes = data => async dispatch => {
    dispatch({
        type: ACTIONS.TIME.SET_TIMES,
        timeType: data
    });
};
