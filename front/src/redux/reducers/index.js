import { combineReducers } from "redux";
import timesReducer from "./timesReducer";

const rootReducer = combineReducers({
  timesReducer
});

export default rootReducer;
