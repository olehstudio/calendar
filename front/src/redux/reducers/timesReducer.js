import ACTIONS from '../actions/actionTypes';

const defaultState = {};

export default function (state = defaultState, action){
    switch (action.type){
        case ACTIONS.TIME.SET_TIMES:
            return {
                ...state,
                [action.timeType.name]: action.timeType.value,
            }
        default:
            return state
    }
}
