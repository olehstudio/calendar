import React, { useEffect, useState } from 'react';
import { Button } from 'antd';
import styled from "styled-components";
import axios from "axios";
import moment from "moment";
import { connect } from "react-redux";

import Stopwatch from "./Stopwatch";
import { currentDateToID } from "../utils/utils";

const TimerWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 0 25px;
`;

function Timer(props) {
    const [startedStatus, setStartedStatus] = useState(false);
    const [buttonStatus, setButtonStatus] = useState(false); // Some refactoring in future
    const [initialStatus, setInitialStatus] = useState(false);
    const [initialValue, setInitialValue] = useState({});

    const handleClick = (e) => {
        setStartedStatus(!startedStatus);
    };

    const handleStopTimer = () => {
        axios.put('http://localhost:3001/timer', {
            status: startedStatus,
            timeType: props.type,
            startTime: 0
        })
            .then(function (response) {
                setInitialStatus(false);
                setInitialValue({});
                props.setMonth(0);
            });

        axios.put('http://localhost:3001/record', {
            dateID: currentDateToID(),
            timeSpent: props.timesData[props.type],
            timeType: props.type
        })
            .then(function (response) {
                setInitialStatus(false);
                setInitialValue({});
            });
    };

    useEffect(() => {
        if (startedStatus && !initialStatus) {
            axios.put('http://localhost:3001/timer', {
                status: startedStatus,
                timeType: props.type,
                startTime: moment().unix()
            })
                .then(function (response) {
                    setInitialStatus(true);
                });
        } else if (!startedStatus && !initialStatus) {
            axios.get(`http://localhost:3001/timers/${props.type}`, {
                status: startedStatus,
                timeType: props.type
            })
                .then(function (response) {
                    if (response.data.status) {
                        const differenceInSeconds = moment().unix() - response.data.startTime;
                        setInitialValue({
                            hours: Math.trunc(differenceInSeconds / 3600),
                            minutes: Math.trunc(differenceInSeconds / 60),
                            seconds: differenceInSeconds % 60
                        });
                        setInitialStatus(true);
                        setStartedStatus(true);
                    }
                });
        } else if (!startedStatus && initialStatus) {
            handleStopTimer();
        }
    }, [startedStatus]);

    return (
        <TimerWrapper>
            <Button type="primary" loading={buttonStatus} onClick={handleClick}>{startedStatus ? "Stop" : "Start"} {props.type}</Button>
            {startedStatus && <Stopwatch initialValue={initialValue} type={props.type} />}
        </TimerWrapper>
    );
}

const mapStateToProps = state => {
    return {
        timesData: state.rootReducer.timesReducer
    };
};

export default connect(mapStateToProps, null)(Timer);
