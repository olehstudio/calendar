import React from 'react';
import { DatePicker, Form, Input, Button, Select } from 'antd';
import styled from "styled-components";
import axios from "axios";
import moment from "moment";

import { convertDateToID } from "../utils/utils";

const CustomAddTime = styled(Form)`
    max-width: 480px;
    margin: 35px auto 0;
`;

const DatePickerStyled = styled(DatePicker)`
    width: 100%;
`;

const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

const AddForm = (props) => {
    const [form] = Form.useForm();
    
    const onFinish = values => {
        axios.put('http://localhost:3001/record', {
            dateID: convertDateToID(values.date),
            timeSpent: Number(values.time),
            timeType: values.timeType,
            action: values.action === "add" ? true : false 
        })
            .then(function (response) {
                props.setMonth(0);
                form.resetFields();
            });
    };

    return (
        <CustomAddTime
            {...layout}
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            form={form}
        >
            <Form.Item
                label="Time Type"
                name="timeType"
                rules={[{ required: true, message: 'Please select the Time Type' }]}
            >
                <Select>
                    <Select.Option value="job">Job</Select.Option>
                    <Select.Option value="learning">Learning</Select.Option>
                    <Select.Option value="work">Work</Select.Option>
                </Select>
            </Form.Item>

            <Form.Item
                label="Action"
                name="action"
                rules={[{ required: true, message: 'Please select an action' }]}
            >
                <Select>
                    <Select.Option value="add">Add</Select.Option>
                    <Select.Option value="remove">Remove</Select.Option>
                </Select>
            </Form.Item>

            <Form.Item
                label="Time"
                name="time"
                rules={[{ required: true, message: 'Please input time in seconds' }]}
                placeholder="Time in seconds"
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Date"
                name="date"
                rules={[{ required: true, message: 'Please input Date ID' }]}
            >
                <DatePickerStyled format="DD/MM/YYYY" />
            </Form.Item>

            <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit">Submit</Button>
            </Form.Item>
        </CustomAddTime>
    );
};

export default AddForm;
