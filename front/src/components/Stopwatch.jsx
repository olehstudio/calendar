import React from 'react';
import ReactStopwatch from 'react-stopwatch';
import { connect } from "react-redux";

import { setTimes } from "../redux/actions/index"

const Stopwatch = (props) => {
    return (
        <ReactStopwatch
            seconds={props.initialValue.seconds || 0}
            minutes={props.initialValue.minutes || 0}
            hours={props.initialValue.hours || 0}
            onChange={({ hours, minutes, seconds }) => {
                props.setTimes({
                    name: props.type,
                    value: hours * 3600 + minutes * 60 + seconds
                })
            }}
            onCallback={() => console.log('Finish')}
            render={({ formatted }) => {
                return (
                    <>
                        {formatted}
                    </>
                );
            }}
        />
    );
};

const mapDispatchToProps = {
    setTimes
};

export default connect(null, mapDispatchToProps)(Stopwatch);
