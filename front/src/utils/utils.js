import moment from "moment";

export const currentDateToID = () => {
    return Number(moment().format("YYYYMMDD"));
};

export const convertDateToID = momentDate => {
    return Number(momentDate.format("YYYYMMDD"));
};

export const firstLetterUppercase = word => {
    return word.replace(/^./, word[0].toUpperCase())
};