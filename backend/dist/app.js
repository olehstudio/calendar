"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var cors = require("cors");
var bodyParser = require("body-parser");
var typeorm_1 = require("typeorm");
var Record_1 = require("./entity/Record");
var Timer_1 = require("./entity/Timer");
typeorm_1.createConnection().then(function (connection) {
    var recordRepository = connection.getRepository(Record_1.Record);
    var timerRepository = connection.getRepository(Timer_1.Timer);
    var app = express();
    app.use(cors());
    app.use(bodyParser.json());
    app.get("/records", function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var records;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, recordRepository.find()];
                    case 1:
                        records = _a.sent();
                        res.json(records);
                        return [2 /*return*/];
                }
            });
        });
    });
    app.get("/records/:month", function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var results;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, recordRepository.find({
                            where: {
                                dateID: typeorm_1.Like("%" + req.params.month + "%")
                            }
                        })];
                    case 1:
                        results = _a.sent();
                        return [2 /*return*/, res.send(results)];
                }
            });
        });
    });
    app.put("/record", function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var results, previousRecord, newRecord;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, recordRepository.findOne({
                            where: {
                                dateID: req.body.dateID,
                                timeType: req.body.timeType
                            }
                        })];
                    case 1:
                        previousRecord = _a.sent();
                        if (!previousRecord) return [3 /*break*/, 3];
                        if (req.body.action) {
                            previousRecord["timeSpent"] = previousRecord.timeSpent + req.body.timeSpent;
                        }
                        else {
                            previousRecord["timeSpent"] = previousRecord.timeSpent + req.body.timeSpent;
                        }
                        return [4 /*yield*/, recordRepository.save(previousRecord)];
                    case 2:
                        results = _a.sent();
                        return [2 /*return*/, res.send(results)];
                    case 3: return [4 /*yield*/, recordRepository.create(req.body)];
                    case 4:
                        newRecord = _a.sent();
                        return [4 /*yield*/, recordRepository.save(newRecord)];
                    case 5:
                        results = _a.sent();
                        return [2 /*return*/, res.send(results)];
                }
            });
        });
    });
    app.delete("/record", function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, recordRepository.delete({
                            dateID: req.body.dateID,
                            timeType: req.body.timeType
                        })];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, res.send(result)];
                }
            });
        });
    });
    app.get("/timers", function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var timers;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, timerRepository.find()];
                    case 1:
                        timers = _a.sent();
                        res.json(timers);
                        return [2 /*return*/];
                }
            });
        });
    });
    app.get("/timers/:timeType", function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var timer;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, timerRepository.findOne({ where: { timeType: req.params.timeType } })];
                    case 1:
                        timer = _a.sent();
                        res.json(timer);
                        return [2 /*return*/];
                }
            });
        });
    });
    app.put("/timer", function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, status_1, timeType, startTime, timerRow, result, e_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 3, , 4]);
                        _a = req.body, status_1 = _a.status, timeType = _a.timeType, startTime = _a.startTime;
                        return [4 /*yield*/, timerRepository.findOne({ where: { timeType: timeType } })];
                    case 1:
                        timerRow = _b.sent();
                        timerRow = __assign(__assign({}, timerRow), { status: status_1, startTime: startTime });
                        return [4 /*yield*/, timerRepository.save(timerRow)];
                    case 2:
                        result = _b.sent();
                        return [2 /*return*/, res.send(result)];
                    case 3:
                        e_1 = _b.sent();
                        throw new Error(e_1);
                    case 4: return [2 /*return*/];
                }
            });
        });
    });
    app.listen(3001);
});
