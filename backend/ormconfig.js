module.exports = {
    "entities": [
        __dirname + "/dist/entity/*.js"
    ],
    "seeds": ['./src/seeds/*.seed.ts'],
    "logging": true,
    "synchronize": true
}