import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';

export default class CreateTimer implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        await connection
            .manager
            .createQueryBuilder()
            .insert()
            .into('timer')
            .values([
                { status: false, timeType: 'job', startTime: '0' },
                { status: false, timeType: 'learning', startTime: '0' },
                { status: false, timeType: 'work', startTime: '0' }
            ])
            .execute()
    }
}