import * as express from "express";
import * as cors from "cors";
import { Request, Response } from "express";
import * as bodyParser from "body-parser";
import { createConnection, Like } from "typeorm";
import { Record } from "./entity/Record";
import { Timer } from "./entity/Timer";

createConnection().then(connection => {
    const recordRepository = connection.getRepository(Record);
    const timerRepository = connection.getRepository(Timer);

    const app = express();
    app.use(cors())
    app.use(bodyParser.json());

    app.get("/records", async function (req: Request, res: Response) {
        const records = await recordRepository.find();
        res.json(records);
    });

    app.get("/records/:month", async function (req: Request, res: Response) {
        const results = await recordRepository.find({
            where: {
                dateID: Like(`%${req.params.month}%`)
            }
        });
        return res.send(results);
    });

    app.put("/record", async function (req: Request, res: Response) {
        let results;
        const previousRecord = await recordRepository.findOne({ 
            where: { 
                dateID: req.body.dateID,
                timeType: req.body.timeType
            } 
        });

        // Splitted functionality
        // var currentTime = new Date(),
        //     midnight = new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 0, 0, 0),
        //     difference = Math.trunc((currentTime.getTime() - midnight.getTime()) / 1000);

        if (previousRecord) {
            if (req.body.action) {
                previousRecord["timeSpent"] = previousRecord.timeSpent + req.body.timeSpent;
            } else {
                previousRecord["timeSpent"] = previousRecord.timeSpent + req.body.timeSpent;    
            }
            
            results = await recordRepository.save(previousRecord);
            return res.send(results);
        } else {
            const newRecord = await recordRepository.create(req.body);
            results = await recordRepository.save(newRecord);
            return res.send(results);
        }
    });

    app.delete("/record", async function(req: Request, res: Response) {
        const result = await recordRepository.delete({ 
            dateID: req.body.dateID,
            timeType: req.body.timeType
        } );
        
        return res.send(result);
    });

    app.get("/timers", async function (req: Request, res: Response) {
        const timers = await timerRepository.find();
        res.json(timers);
    });

    app.get("/timers/:timeType", async function (req: Request, res: Response) {
        const timer = await timerRepository.findOne({ where: { timeType: req.params.timeType } });
        res.json(timer);
    });

    app.put("/timer", async function (req: Request, res: Response) {
        try {
            const { status, timeType, startTime } = req.body;
            let timerRow = await timerRepository.findOne({ where: { timeType } });
            timerRow = { ...timerRow, status, startTime }
            const result = await timerRepository.save(timerRow);
            return res.send(result);
        } catch (e) {
            throw new Error(e);
        }
    });

    app.listen(3001);
});