import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Record {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    dateID: number;

    @Column()
    timeSpent: number;

    @Column()
    timeType: string;
}