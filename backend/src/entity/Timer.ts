import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Timer {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    status: boolean;

    @Column()
    timeType: string;

    @Column()
    startTime: string;
}